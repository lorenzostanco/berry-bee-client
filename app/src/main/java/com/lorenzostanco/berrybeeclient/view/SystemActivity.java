package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

import com.lorenzostanco.berrybeeclient.BerryBeeActivity;
import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.adapter.SystemAdapter;
import com.lorenzostanco.berrybeeclient.model.Disk;
import com.lorenzostanco.berrybeeclient.model.Process;
import com.lorenzostanco.utils.JSONWebService;

public class SystemActivity extends BerryBeeActivity {
	
	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_system);

		showEmpty(true);
		
		// Setup list
		final ListView list = (ListView)findViewById(R.id.list);
		final SystemAdapter adapter = new SystemAdapter(this);
		list.setAdapter(adapter);

		// On errors, show the empty message
		getBerryBeeWebService().addOnErrorListener(new JSONWebService.OnErrorListener() {
			@Override public void onError(String url, String type, String message) {
				showEmpty(true);
			}
		});
		
		// On success display system info
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {	
			@Override public void onSuccess(String url, JSONObject response) {
				adapter.clearAll();
				try {
					
					// System
					adapter.setCpu((float)response.getDouble("cpu"));
					adapter.setMemoryUsed(response.getJSONObject("memory").getInt("used"));
					adapter.setMemoryTotal(response.getJSONObject("memory").getInt("total"));
					adapter.setUptime(response.getInt("uptime"));
					
					// Load avarage
					JSONArray loadAvg = response.getJSONArray("loadavg");
					adapter.setLoadAvg((float)loadAvg.getDouble(0), (float)loadAvg.getDouble(1), (float)loadAvg.getDouble(2));

					// Disks
					JSONArray disks = response.getJSONArray("disks");
					for (int i = 0; i < disks.length(); i++) adapter.addDisk(new Disk(disks.getJSONObject(i)));

					// Processes
					JSONArray processes = response.getJSONArray("processes");
					for (int i = 0; i < processes.length(); i++) adapter.addProcess(new Process(processes.getJSONObject(i)));

					// Custom commands
					JSONArray customCommands = response.optJSONArray("customcommands");
					if (customCommands != null) for (int i = 0; i < customCommands.length(); i++) {
						JSONObject customCommandJSON = customCommands.getJSONObject(i);
						adapter.addCustomCommand(customCommandJSON.getString("label"), customCommandJSON.getString("output"));
					}
					
				} catch (JSONException x) {
					showErrorDialog(getString(R.string.error_response_unexpected));
				}
				adapter.notifyDataSetChanged();
				showEmpty(adapter.isEmpty());
			}
		});
		
	}
	
	public void showEmpty(boolean empty) {
		findViewById(R.id.list).setVisibility(empty ? View.GONE : View.VISIBLE);
		findViewById(R.id.empty).setVisibility(empty ? View.VISIBLE : View.GONE);
	}
	
	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.base, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

}
