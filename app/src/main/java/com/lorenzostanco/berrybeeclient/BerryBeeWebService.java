package com.lorenzostanco.berrybeeclient;

import java.util.HashMap;
import java.util.Map;

import android.net.Uri;

import com.lorenzostanco.utils.JSONWebService;

/**
 * Berry bee web service client.
 * Starts requests, read responses and manages event listeners.
 */
public class BerryBeeWebService extends JSONWebService {
	
	private String baseURL;
	private String token;
	
	/** Initializes client with Berry bee base URL and secret token */
	public BerryBeeWebService(String baseURL, String token) {
		this(baseURL, token, 15000);
	}
	
	/** Initializes client with Berry bee base URL and secret token */
	public BerryBeeWebService(String baseURL, String token, int timeout) {
		super(timeout);
		this.baseURL = baseURL;
		this.token = token;
	}
	
	/** Runs the authentication service */
	public void auth(String device, String passphrase) {
		Map<String, String> params = new HashMap<String, String>();
		params.put("device", device);
		params.put("passphrase", passphrase);
		this.requestService("auth", null, params);
	}
	
	/** Runs a new request for services list. */
	public void requestServicesList() {
		this.requestService(null, null, null);
	}

	/** Runs a new request with no command.
	 * @param service Berry bee service */
	public void requestService(String service) {
		this.requestService(service, null, null);
	}

	/** Runs a new request with command but no parameters.
	 * @param service Berry bee service
	 * @param command Service command */
	public void requestService(String service, String command) {
		this.requestService(service, command, null);
	}

	/** Runs a new request.
	 * @param service Berry bee service
	 * @param command Service command
	 * @param params A map of parameters */
	public void requestService(String service, String command, Map<String, String> params) {
		
		// Cancel any running request
		this.cancel();
		
		// Try to build URL and launch request
		this.request(getRequestServiceUrl(service, command, params));
		
	}

	/** Return the URL for a specific request.
	 * @param service Berry bee service
	 * @param command Service command
	 * @param params A map of parameters */
	public String getRequestServiceUrl(String service, String command, Map<String, String> params) {
		
		// Build URL
		Uri.Builder builder = Uri.parse(this.baseURL).buildUpon();
		builder.appendQueryParameter("token", this.token);
		if (service != null) builder.appendQueryParameter("service", service);
		if (command != null) builder.appendQueryParameter("command", command);
		if (params != null) for (Map.Entry<String,String> p : params.entrySet()) {
			builder.appendQueryParameter(p.getKey(), p.getValue());
		}
		
		// Try to build URL and launch request
		return builder.build().toString();
		
	}
	
	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}
	
	public void setToken(String token) {
		this.token = token;
	}

}
