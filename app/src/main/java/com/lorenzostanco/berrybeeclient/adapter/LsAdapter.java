package com.lorenzostanco.berrybeeclient.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.model.File;
import com.lorenzostanco.berrybeeclient.model.Folder;

public class LsAdapter extends BaseAdapter {
	
	private LayoutInflater inflater;
	public final static int TYPE_FOLDER = 0;
	public final static int TYPE_FILE = 1;
	public final static int TYPE_FILE_COPYING = 2;

	private List<Folder> folders;
	private List<File> files;
	
	public LsAdapter(Context context) {
		super();
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		folders = new ArrayList<Folder>();
		files = new ArrayList<File>();
	}
	
	@Override public View getView(int position, View convertView, ViewGroup parent) {
		
		// Inflate layout, hide non-used views on complex list item
		int type = getItemViewType(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item_complex, null);
			switch (type) {
				case TYPE_FOLDER:
					convertView.findViewById(R.id.list_item_description).setVisibility(View.GONE);
					convertView.findViewById(R.id.list_item_info).setVisibility(View.GONE);
					((ImageView)convertView.findViewById(R.id.list_item_icon)).setImageResource(R.drawable.ic_folder_open_black_24dp);
					break;
				case TYPE_FILE:
					((ImageView)convertView.findViewById(R.id.list_item_icon)).setImageResource(R.drawable.ic_insert_drive_file_black_24dp);
					((TextView)convertView.findViewById(R.id.list_item_description)).setVisibility(View.GONE);
					SystemAdapter.setProgress(convertView, 1f);
					break;
				case TYPE_FILE_COPYING:
					((ImageView)convertView.findViewById(R.id.list_item_icon)).setImageResource(R.drawable.ic_insert_drive_file_black_24dp);
					break;
			}
		}
		
		// Display data
		switch (type) {
			case TYPE_FOLDER:
				Folder folder = (Folder)getItem(position);
				((TextView)convertView.findViewById(R.id.list_item_text)).setText(folder.getName());
				break;
			case TYPE_FILE:
				File file = (File)getItem(position);
				((TextView)convertView.findViewById(R.id.list_item_text)).setText(file.getName());
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(file.getHumanReadableSize());
				break;
			case TYPE_FILE_COPYING:
				File fileC = (File)getItem(position);
				((TextView)convertView.findViewById(R.id.list_item_text)).setText(fileC.getName());
				((TextView)convertView.findViewById(R.id.list_item_description)).setText(String.format("Copying " + fileC.getHumanReadableSize() + " of " + fileC.getHumanReadableCopying() + "..."));
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(String.format("%.0f %%", fileC.getCopyingProgressPercent()));
				SystemAdapter.setProgress(convertView, fileC.getCopyingProgress());
				break;
		}
		
		return convertView;
	}

	@Override public int getViewTypeCount() {
		return 3;
	}
	
	@Override public int getItemViewType(int position) {
		if (position < folders.size()) return TYPE_FOLDER;
		return files.get(position - folders.size()).isCopying() ? TYPE_FILE_COPYING : TYPE_FILE;
	}

	@Override public int getCount() {
		return folders.size() + files.size();
	}

	@Override public Object getItem(int position) {
		switch (getItemViewType(position)) {
			case TYPE_FOLDER: return folders.get(position);
			default: return files.get(position - folders.size());
		}
	}

	@Override public long getItemId(int position) {
		return position;
	}
	
	public void addFolder(Folder directory) {
		folders.add(directory);
	}
	
	public void addFile(File file) {
		files.add(file);
	}
	
	public List<Folder> getFolders() {
		return folders;
	}
	
	public int getFoldersCount() {
		return folders.size();
	}
	
	public List<File> getFiles() {
		return files;
	}
	
	@Override public boolean isEnabled(int position) {
		int type = getItemViewType(position);
		return (type == TYPE_FILE || type == TYPE_FOLDER);
	}
	
	public void clearAll() {
		folders.clear();
		files.clear();
		notifyDataSetChanged();
	}
	
}
