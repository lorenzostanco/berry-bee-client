package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.utils.JSONWebService;

public class TransmissionActivity extends LsActivity {
	
	private boolean running = false;
	private String webUrl;

	private View launchWeb = null;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.callSuperOnCreate(savedInstanceState);
		setContentView(R.layout.activity_transmission);
		
		// Larger timeout for Transmission commands
		getBerryBeeWebService().setTimeout(30000);
		
		// Hide Transmission status initially
		findViewById(R.id.transmission_wrapper).setVisibility(View.GONE);

		// Launch web button
		launchWeb = findViewById(R.id.launch_web);
		ViewCompat.setElevation(launchWeb, getResources().getDimension(R.dimen.floatingbutton_elevation));
		launchWeb.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				openWeb();
			}
		});
		
		// Manage running stuffs
		setRunning(false);
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {
			@Override public void onSuccess(String url, JSONObject response) {

				// Now I can show status bar
				findViewById(R.id.transmission_wrapper).setVisibility(View.VISIBLE);
				
				// Set running status
				setRunning(response.optBoolean("running", false));
				if (isRunning()) webUrl = response.optString("weburl");
				
				// Display Transmission status if running
				if (isRunning()) try {
					String upSpeed = response.getString("upspeed");
					String downSpeed = response.getString("downspeed");
					int torrentsCount = response.getInt("torrentscount");
					((TextView)findViewById(R.id.list_item_description)).setText(torrentsCount + " torrents - Down: " + downSpeed + " KB/s - Up: " + upSpeed + " KB/s");
					findViewById(R.id.list_item_description).setVisibility(View.VISIBLE);
				} catch (JSONException x) {
					findViewById(R.id.list_item_description).setVisibility(View.GONE);
				}
				
			}
		});

		// Setup standard Ls stuff
		setupLsList();
		
	}

	public void openWeb() {
		if (isRunning() && this.webUrl != null && !this.webUrl.trim().equals("")) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.webUrl));
			startActivity(intent);
		}
	}

	public void start() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "start");
	}
	
	public void stop() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "stop");
	}
	
	public void setRunning(boolean running) {
		this.running = running;
		
		// Refresh Transmission status
		((TextView)findViewById(R.id.list_item_text)).setText(isRunning() ? R.string.transmission_running : R.string.transmission_not_running);
		findViewById(R.id.list_item_description).setVisibility(isRunning() ? View.VISIBLE : View.GONE);

		// Launch web button
		launchWeb.setVisibility(isRunning() ? View.VISIBLE : View.GONE);
		
		// Refresh menu
		if (this.getMenu() != null) onPrepareOptionsMenu(this.getMenu());
		
	}

	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_transmission_start).setVisible(!isRunning());
		menu.findItem(R.id.action_transmission_stop).setVisible(isRunning());
		return true;
	}

	public boolean isRunning() {
		return running;
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.transmission, menu);
		super.callSuperOnCreateOptionsMenu(menu);
		return true;
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_transmission_start:
				start();
				return true;
			case R.id.action_transmission_stop:
				stop();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
}
