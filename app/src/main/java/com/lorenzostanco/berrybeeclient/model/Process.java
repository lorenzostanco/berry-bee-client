package com.lorenzostanco.berrybeeclient.model;

import org.json.JSONException;
import org.json.JSONObject;

public class Process {
	
	private float cpu;
	private float memory;
	private String command;
	
	public Process(float cpu, float memory, String command) {
		super();
		this.cpu = cpu;
		this.memory = memory;
		this.command = command;
	}

	public Process(JSONObject o) throws JSONException {
		this((float)o.getDouble("cpu"), (float)o.getDouble("memory"), o.getString("command"));
	}

	public String getCommand() {
		return command;
	}
	
	public float getCpuPercent() {
		return cpu;
	}
	
	public float getMemoryPercent() {
		return memory;
	}

}
