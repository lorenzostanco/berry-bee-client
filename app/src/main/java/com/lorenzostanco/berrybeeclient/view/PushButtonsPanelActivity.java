package com.lorenzostanco.berrybeeclient.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lorenzostanco.berrybeeclient.BerryBeeActivity;
import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.utils.JSONWebService;
import com.lorenzostanco.utils.StringUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PushButtonsPanelActivity extends BerryBeeActivity {

	LinearLayout rows;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_push_buttons_panel);

		rows = (LinearLayout)findViewById(R.id.buttons_rows);

		// Larger timeout for custom commands
		getBerryBeeWebService().setTimeout(30000);

		// On errors, show the empty message
		getBerryBeeWebService().addOnErrorListener(new JSONWebService.OnErrorListener() {
			@Override public void onError(String url, String type, String message) {
				showEmpty(true);
			}
		});

		// On success display log
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {
			@Override public void onSuccess(String url, JSONObject response) {

				rows.removeAllViews();
				LinearLayout row = null; // Current row

				boolean error = false;
				try {

					// After command execution
					if (response.has("retval")) {
						final int retval = response.getInt("retval");
						Toast.makeText(getBerryBeeActivity(), String.format(getString(retval == 0 ? R.string.push_buttons_command_success : R.string.push_buttons_command_errors), retval), Toast.LENGTH_LONG).show();
					}
					if (response.has("output")) {
						final String output = response.getString("output").trim();
						if (!output.isEmpty()) {
							final AlertDialog.Builder dialog = new AlertDialog.Builder(getBerryBeeActivity());
							dialog.setTitle(R.string.push_buttons_command_output);
							dialog.setMessage(output);
							dialog.setCancelable(true);
							dialog.setPositiveButton(R.string.close, null);
							dialog.show();
						}
					}

					final int columns = response.getInt("columns");
					final JSONArray jsonButtons = response.getJSONArray("buttons");
					for (int i = 0; i < jsonButtons.length(); i++) {
						JSONObject jsonButton = jsonButtons.getJSONObject(i);

						// New row?
						if (i % columns == 0) {
							row = new LinearLayout(PushButtonsPanelActivity.this);
							row.setOrientation(LinearLayout.HORIZONTAL);
							row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
							rows.addView(row);
						}

						// Create button view
						final int buttonColor = Color.parseColor(jsonButton.getString("color"));
						final String buttonLabel = jsonButton.getString("label");
						final String commandName = jsonButton.getString("name");
						final boolean confirm = jsonButton.optBoolean("confirm", false);
						final Button button = new Button(PushButtonsPanelActivity.this);
						button.setText(buttonLabel);
						button.getBackground().setColorFilter(buttonColor, PorterDuff.Mode.MULTIPLY);
						button.setTextColor(getTextColorForBackground(buttonColor));

						// Click to execute command
						button.setOnClickListener(new View.OnClickListener() {
							@Override public void onClick(View v) {
								if (!confirm) {
									executeCommand(commandName);
								} else {
									final AlertDialog.Builder dialog = new AlertDialog.Builder(getBerryBeeActivity());
									dialog.setMessage(String.format(getString(R.string.push_buttons_confirm), buttonLabel));
									dialog.setCancelable(true);
									dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
										@Override public void onClick(DialogInterface dialog, int which) {
											executeCommand(commandName);
										}
									});
									dialog.setNegativeButton(R.string.no, null);
									dialog.show();
								}
							}
						});

						// Add the button into the row
						button.setLayoutParams(new LinearLayout.LayoutParams(0, getResources().getDimensionPixelSize(R.dimen.push_buttons_height), 1));
						row.addView(button);

					}

				} catch (JSONException x) {
					error = true;
					showErrorDialog(getString(R.string.error_response_unexpected));
				}

				showEmpty(error || rows.getChildCount() == 0);

			}
		});


	}

	public void executeCommand(final String commandName) {
		getBerryBeeWebService().requestService(getServiceName(), commandName);
	}

	public void showEmpty(boolean empty) {
		findViewById(R.id.scroll).setVisibility(empty ? View.GONE : View.VISIBLE);
		findViewById(R.id.empty).setVisibility(empty ? View.VISIBLE : View.GONE);
	}

	// Returns black or white depending on better contrast with a background color
	// http://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
	public int getTextColorForBackground(int backgroundColor) {
		if ((Color.red(backgroundColor) * 0.299 + Color.green(backgroundColor) * 0.587 + Color.blue(backgroundColor) * 0.114) > 186.0) {
			return Color.BLACK;
		} else {
			return Color.WHITE;
		}
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.base, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

}
