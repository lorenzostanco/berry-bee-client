package com.lorenzostanco.berrybeeclient.model;

import org.json.JSONException;
import org.json.JSONObject;

import com.lorenzostanco.utils.StringUtils;

public class Disk {
	
	private String filter;
	private String mountpoint;
	private long used;
	private long total;
	private String type; 
	
	public Disk(String filter, String mountpoint, long used, long total, String type) {
		super();
		this.filter = filter;
		this.mountpoint = mountpoint;
		this.used = used;
		this.total = total;
		this.type = type;
	}

	public Disk(JSONObject o) throws JSONException {
		this(o.getString("filter"), o.getString("mountpoint"), o.getLong("used"), o.getLong("total"), o.getString("type"));
	}

	public String getFilter() {
		return filter;
	}
	
	public String getMountpoint() {
		return mountpoint;
	}

	public long getUsed() {
		return used;
	}

	public long getFree() {
		return total - used;
	}
	
	public long getTotal() {
		return total;
	}

	public String getHumanReadableUsed() {
		return StringUtils.humanReadableByteCount(used * 1024, "@@#");
	}

	public String getHumanReadableFree() {
		return StringUtils.humanReadableByteCount(getFree() * 1024, "@@#");
	}
	
	public String getHumanReadableTotal() {
		return StringUtils.humanReadableByteCount(total * 1024, "@@#");
	}
	
	public float getUsage() {
		return (float)used / (float)total;
	}
	
	public float getUsagePercent() {
		return getUsage() * 100;
	}
	
	public String getType() {
		return type;
	}
	
}
