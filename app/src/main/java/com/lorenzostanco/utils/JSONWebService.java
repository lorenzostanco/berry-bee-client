package com.lorenzostanco.utils;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import android.os.AsyncTask;

/**
 * JSON web service client.
 * Starts requests, read responses and manages event listeners.
 */
public class JSONWebService {
	
	private int timeout;
	
	// Current connection
	private HttpURLConnection connection = null;
	private AsyncTask<Void, Void, Object> asyncTask;

	// Listeners
	private List<OnRequestListener> onRequestListeners;
	private List<OnCancelListener> onCancelListeners;
	private List<OnCompleteListener> onCompleteListeners;
	private List<OnSuccessListener> onSuccessListeners;
	private List<OnErrorListener> onErrorListeners;
	
	/** Initializes client */
	public JSONWebService() {
		this(15000);
	}
	
	/** Initializes client specifying connection/read timeout (in milliseconds) */
	public JSONWebService(int timeout) {
		this.setTimeout(timeout);
		this.onRequestListeners = new ArrayList<OnRequestListener>();
		this.onCancelListeners = new ArrayList<OnCancelListener>();
		this.onCompleteListeners = new ArrayList<OnCompleteListener>();
		this.onSuccessListeners = new ArrayList<OnSuccessListener>();
		this.onErrorListeners = new ArrayList<OnErrorListener>();
	}
	
	/** Cancel current connection, if any */
	public void cancel() {
		if (this.asyncTask != null) this.asyncTask.cancel(true);
		if (this.isRunning()) {
			String url = this.connection.getURL().toString();
			this.connection.disconnect();
			this.connection = null;
			for (OnCancelListener l : this.onCancelListeners) l.onCancel(url);
		}
	}

	/** Returns true if client is running a request */
	public boolean isRunning() {
		return this.connection != null;
	}

	/** Fired when request starts */
	public void addOnRequestListener(OnRequestListener l) {
		this.onRequestListeners.add(l);
	}

	/** Fired when request is canceled */
	public void addOnCancelListener(OnCancelListener l) {
		this.onCancelListeners.add(l);
	}

	/** Fired when request ends (with success or with error) */
	public void addOnCompleteListener(OnCompleteListener l) {
		this.onCompleteListeners.add(l);
	}

	/** Fired when request ends with success */
	public void addOnSuccessListener(OnSuccessListener l) {
		this.onSuccessListeners.add(l);
	}

	/** Fired when request ends with errors */
	public void addOnErrorListener(OnErrorListener l) {
		this.onErrorListeners.add(l);
	}
	
	/** Sets request timeout in milliseconds */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/** Performs request on URL. Used by other public request() methods */
	public void request(final String url) {
		
		// Cancel any running request
		this.cancel();
		
		// Run an asynchronous task in background
		this.asyncTask = new AsyncTask<Void, Void, Object>() {
			
			// Before running request, fire onRequest event
			protected void onPreExecute() {
				for (OnRequestListener l : onRequestListeners) l.onRequest(url);
			};

			// Background task
			@Override protected Object doInBackground(Void... params) {
				try {

					// Open and setup connection
					connection = (HttpURLConnection) new URL(url).openConnection();
					connection.setConnectTimeout(timeout);
					connection.setReadTimeout(timeout);

					// Read input stream
					InputStream in = connection.getInputStream();
					InputStreamReader inr = new InputStreamReader(in, "UTF-8");
					int read = 0;
					char[] buffer = new char[512];
					StringBuffer stringBuffer = new StringBuffer();
					while ((read = inr.read(buffer)) > 0) stringBuffer.append(buffer, 0, read);

					// On success, result is the JSON
					inr.close();
					return new JSONObject(stringBuffer.toString());

				} catch (Exception e) {

					// On errors, result is the exception
					return e;

				}
			}
			
			// After running background task...
			protected void onPostExecute(Object result) {
				
				// Request is complete, errors or not!
				for (OnCompleteListener l : onCompleteListeners) l.onComplete(url);
				
				// Close connection
				if (connection != null) {
					connection.disconnect();
					connection = null;
				}
				
				// JSON read?
				if (result instanceof JSONObject) {
					
					// Cast JSON response
					JSONObject response = (JSONObject)result;
					
					// Is error?
					if (response.optBoolean("error", false)) {
						for (OnErrorListener l : onErrorListeners) l.onError(url, 
							response.optString("errorType", "Exception"), 
							response.optString("errorMessage", "Unknown exception")
						);
						
					// Success?
					} else {
						for (OnSuccessListener l : onSuccessListeners) l.onSuccess(url, response);
					}
					
				}
				
				// Exception?
				if (result instanceof Exception) {
					Exception e = (Exception)result;
					for (OnErrorListener l : onErrorListeners) l.onError(url, e.getClass().getSimpleName(), e.getMessage());
				}
				
			};
			
		};
		
		// Go
		this.asyncTask.execute();
		
	}
	
	// Listeners
	public interface OnRequestListener { public void onRequest(String url); }
	public interface OnCancelListener { public void onCancel(String url); }
	public interface OnCompleteListener { public void onComplete(String url); }
	public interface OnSuccessListener { public void onSuccess(String url, JSONObject response); }
	public interface OnErrorListener { public void onError(String url, String type, String message); }

}
