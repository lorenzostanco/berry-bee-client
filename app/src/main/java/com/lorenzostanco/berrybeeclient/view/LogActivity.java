package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.lorenzostanco.berrybeeclient.BerryBeeActivity;
import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.utils.JSONWebService;

public class LogActivity extends BerryBeeActivity {

	boolean singleLine = false;
	boolean monospace = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log);
		
		setupLogOptions();
		setupLogList();
		
	}
	
	public void setupLogOptions() {
		
		// Get log preferences
		SharedPreferences preferences = getPreferences();
		singleLine = preferences.getBoolean("log_single_line", false);
		monospace = preferences.getBoolean("log_monospace", false);
		
		// Update menu
		if (this.getMenu() != null) onPrepareOptionsMenu(this.getMenu());
		
	}
	
	public void setupLogList() {

		showEmpty(true);
		
		// Setup list
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.list_item_logline, R.id.list_item_text) {
			@Override public View getView(int position, View convertView, ViewGroup parent) {
				
				// Alternate row background
				View v = super.getView(position, convertView, parent);
				v.setBackgroundColor(Color.argb(position % 2 == 0 ? 16 : 0, 0, 0, 0));
				
				// Monospace and single line
				TextView t = (TextView)v.findViewById(R.id.list_item_text);
				if (convertView == null && (singleLine || monospace)) {
					if (monospace) t.setTypeface(Typeface.MONOSPACE);
					if (singleLine) t.setSingleLine(true);
				}
				
				// Highlight key words
				highlight(t);
				
				return v;
				
			}
			@Override public int getViewTypeCount() {
				return 4;
			}
			@Override public int getItemViewType(int position) {
				if (singleLine && monospace) return 3;
				if (singleLine && !monospace) return 2;
				if (!singleLine && monospace) return 1;
				return 0;
			}
		};
		final ListView list = (ListView)findViewById(R.id.list);
		list.setAdapter(adapter);
		
		// Copy to clipboard on long-click
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				copyToClipboard(adapter.getItem(position));
				return true;
			}
		});

		// On errors, show the empty message
		getBerryBeeWebService().addOnErrorListener(new JSONWebService.OnErrorListener() {
			@Override public void onError(String url, String type, String message) {
				showEmpty(true);
			}
		});
		
		// On success display log
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {	
			@Override public void onSuccess(String url, JSONObject response) {
				adapter.clear();
				try { 
					JSONArray lines = response.getJSONArray("lines");
					for (int i = 0; i < lines.length(); i++) adapter.add(lines.getString(i));
				} catch (JSONException x) {
					showErrorDialog(getString(R.string.error_response_unexpected));
				}
				adapter.notifyDataSetChanged();
				showEmpty(adapter.isEmpty());
				if (!adapter.isEmpty()) list.setSelection(adapter.getCount() - 1);
			}
		});
		
	}

	/** Calls super method only, do not setup content view */
	protected void callSuperOnCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	/** Highlights keywords in text using colors */
	private void highlight(TextView t) {
		t.setText(Html.fromHtml(
			TextUtils.htmlEncode(t.getText().toString())
				.replaceAll("(?i)(\\berror[a-z]*\\b|\\[e\\])", "<font color=\"#cc0000\"><b>$0</b></font>")
				.replaceAll("(?i)(\\bwarn[a-z]*\\b|\\[w\\])", "<font color=\"#FF8800\"><b>$0</b></font>")
				.replaceAll("(?i)(\\binfo[a-z]*\\b|\\bnotice[a-z]*\\b|\\bupdate[a-z]*\\b|\\[i\\])", "<font color=\"#669900\"><b>$0</b></font>")
				.replaceAll("(?i)(\\b[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\b|\\[[dln]\\])", "<font color=\"#0099CC\"><b>$0</b></font>")
		));
	}
	
	public void showEmpty(boolean empty) {
		findViewById(R.id.list).setVisibility(empty ? View.GONE : View.VISIBLE);
		findViewById(R.id.empty).setVisibility(empty ? View.VISIBLE : View.GONE);
	}

	@SuppressWarnings("deprecation") @SuppressLint("NewApi") private void copyToClipboard(String text) {
		if (android.os.Build.VERSION.SDK_INT < 11) {
	        android.text.ClipboardManager clipboard = (android.text.ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
	        clipboard.setText(text);
	    } else {
	        android.content.ClipboardManager clipboard = (android.content.ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
	        android.content.ClipData clip = android.content.ClipData.newPlainText(text, text);
	        clipboard.setPrimaryClip(clip);
	    }
	    Toast.makeText(this, R.string.clipboard_text_copied, Toast.LENGTH_SHORT).show();
	}
	
	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_log_single_line).setChecked(singleLine);
		menu.findItem(R.id.action_log_monospace).setChecked(monospace);
		return true;
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.log, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_log_single_line:
				getPreferences().edit().putBoolean("log_single_line", !item.isChecked()).commit();
				setupLogOptions();
				((ListView)findViewById(R.id.list)).invalidateViews();
				return true;
			case R.id.action_log_monospace:
				getPreferences().edit().putBoolean("log_monospace", !item.isChecked()).commit();
				setupLogOptions();
				((ListView)findViewById(R.id.list)).invalidateViews();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	/** Calls super method only, do not inflate menu */
	public boolean callSuperOnCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}
	
}
