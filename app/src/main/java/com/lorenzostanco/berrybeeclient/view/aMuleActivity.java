package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.utils.JSONWebService;

public class aMuleActivity extends LsActivity {
	
	private boolean running = false;
	private boolean runningWeb = false;
	private String webUrl;
	
	private View launchWeb = null;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.callSuperOnCreate(savedInstanceState);
		setContentView(R.layout.activity_amule);
		
		// Larger timeout for aMule commands
		getBerryBeeWebService().setTimeout(30000);
		
		// Hide aMule status initially
		findViewById(R.id.amule_wrapper).setVisibility(View.GONE);

		// Launch web button
		launchWeb = findViewById(R.id.launch_web);
		ViewCompat.setElevation(launchWeb, getResources().getDimension(R.dimen.floatingbutton_elevation));
		launchWeb.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View v) {
				openWeb();
			}
		});
		
		// Manage running stuffs
		setRunning(false);
		setRunningWeb(false);
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {
			@Override public void onSuccess(String url, JSONObject response) {

				// Now I can show status bar
				findViewById(R.id.amule_wrapper).setVisibility(View.VISIBLE);
				
				// Set running status
				setRunning(response.optInt("amuled", 0) > 0);
				setRunningWeb(response.optInt("amuleweb", 0) > 0);
				if (isRunning()) webUrl = response.optString("weburl");
				
				// Display aMule status if running
				if (isRunning()) try {
					String upSpeed = response.getString("upspeed");
					String downSpeed = response.getString("downspeed");
					((TextView)findViewById(R.id.list_item_description)).setText("Download: " + downSpeed + " - Upload: " + upSpeed);
					findViewById(R.id.list_item_description).setVisibility(View.VISIBLE);
				} catch (JSONException x) {
					findViewById(R.id.list_item_description).setVisibility(View.GONE);
				}
				
			}
		});

		// Setup standard Ls stuff
		setupLsList();
		
	}

	public void openWeb() {
		if (isRunningWeb() && this.webUrl != null && !this.webUrl.trim().equals("")) {
			Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(this.webUrl));
			startActivity(intent);
		}
	}

	public void start() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "start");
	}
	
	public void stop() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "stop");
	}

	public void startWeb() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "startweb");
	}
	
	public void stopWeb() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "stopweb");
	}

	public void setRunning(boolean running) {
		this.running = running;
		
		// Refresh aMule status
		((TextView)findViewById(R.id.list_item_text)).setText(isRunning() ? R.string.amule_running_without_web : R.string.amule_not_running);
		findViewById(R.id.list_item_description).setVisibility(isRunning() ? View.VISIBLE : View.GONE);
		
		// Refresh menu
		if (this.getMenu() != null) onPrepareOptionsMenu(this.getMenu());
		
	}

	public void setRunningWeb(boolean runningWeb) {
		this.runningWeb = runningWeb;

		// Refresh aMule status
		((TextView)findViewById(R.id.list_item_text)).setText(isRunning() ? (isRunningWeb() ? R.string.amule_running_with_web : R.string.amule_running_without_web) : R.string.amule_not_running);
		
		// Launch web button
		launchWeb.setVisibility(isRunningWeb() ? View.VISIBLE : View.GONE);
		
		// Refresh menu
		if (this.getMenu() != null) onPrepareOptionsMenu(this.getMenu());
		
	}
	
	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_amule_start).setVisible(!isRunning());
		menu.findItem(R.id.action_amule_stop).setVisible(isRunning());
		menu.findItem(R.id.action_amule_start_web).setVisible(isRunning() && !isRunningWeb());
		menu.findItem(R.id.action_amule_stop_web).setVisible(isRunningWeb());
		return true;
	}

	public boolean isRunning() {
		return running;
	}

	public boolean isRunningWeb() {
		return runningWeb;
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.amule, menu);
		super.callSuperOnCreateOptionsMenu(menu);
		return true;
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_amule_start:
				start();
				return true;
			case R.id.action_amule_stop:
				stop();
				return true;
			case R.id.action_amule_start_web:
				startWeb();
				return true;
			case R.id.action_amule_stop_web:
				stopWeb();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
}
