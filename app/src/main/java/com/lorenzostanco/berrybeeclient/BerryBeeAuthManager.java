package com.lorenzostanco.berrybeeclient;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences.Editor;

/**
 * Stores and retrieve many combinations of
 * base URL and authentication tokens. 
 */
public class BerryBeeAuthManager {
	
	private List<Token> tokens;

	public BerryBeeAuthManager(Context context) {
		
		// Read the list of auths stored in shared preferences as JSON
		this.tokens = new ArrayList<Token>();
		String tokensJSON = context.getSharedPreferences(BerryBeeActivity.TAG, Context.MODE_PRIVATE).getString("tokens", "[]");
		try {
			JSONArray tokensArray = new JSONArray(tokensJSON);
			for (int i = 0; i < tokensArray.length(); i++) {
				Token token = new Token();
				token.baseURL = tokensArray.getJSONObject(i).getString("baseURL");
				token.token = tokensArray.getJSONObject(i).getString("token");
				this.tokens.add(token);
			}
		} catch (JSONException e) {
			e.printStackTrace(); // This never happens...
		}
		
	}
	
	/** Retrieves all stored combinations of base URL and authentication tokens */
	public List<Token> getTokens() {
		return tokens;
	}

	/** Stores a new combination of base URL and token */
	public void storeToken(String baseURL, String token, Context context) {
		Token t = new Token();
		t.baseURL = baseURL;
		t.token = token;
		this.tokens.add(t);
		this.saveJSON(context);
	}
	
	/** Removes an existing combination of base URL and token */
	public void removeToken(String baseURL, String token, Context context) {
		List<Token> found = new ArrayList<BerryBeeAuthManager.Token>();
		for (Token t : this.tokens) if (t.baseURL.equals(baseURL) && t.token.equals(token)) found.add(t);
		for (Token t : found) this.tokens.remove(t);
		this.saveJSON(context);
	}
	
	// Save tokens in shared preferences as JSON
	private void saveJSON(Context context) {
		JSONArray tokensArray = new JSONArray();
		for (Token u : this.tokens) try {
			JSONObject o = new JSONObject();
			o.put("baseURL", u.baseURL);
			o.put("token", u.token);
			tokensArray.put(o);
		} catch (JSONException e) {
			e.printStackTrace(); // This never happens...
		}
		Editor editor = context.getSharedPreferences(BerryBeeActivity.TAG, Context.MODE_PRIVATE).edit();
		editor.putString("tokens", tokensArray.toString()).commit();
	}
	
	/** Represent a single combination of base URL and token */
	public class Token {
		private String baseURL;
		private String token;
		public String getBaseURL() { return baseURL; }
		public String getToken() { return token; }
	}
	
}
