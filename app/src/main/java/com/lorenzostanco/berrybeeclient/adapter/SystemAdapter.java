package com.lorenzostanco.berrybeeclient.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.model.Disk;
import com.lorenzostanco.berrybeeclient.model.Process;
import com.lorenzostanco.utils.StringUtils;

/**  
 * List:
 *  - (system)
 *  - CPU
 *  - Memory
 *  - Uptime
 *  - Loadvg
 *  - Custom commands...
 *  - (disks)
 *  - Disks...
 *  - (top processes)
 *  - Processes...
 */
public class SystemAdapter extends BaseAdapter {
	
	private LayoutInflater inflater;
	
	private final static int TYPE_SEPARATOR = 0;
	private final static int TYPE_CPU = 1;
	private final static int TYPE_MEMORY = 2;
	private final static int TYPE_UPTIME = 3;
	private final static int TYPE_DISK = 4;
	private final static int TYPE_PROCESS = 5;
	private final static int TYPE_CUSTOM_COMMAND = 6;
	private final static int TYPE_LOADAVG = 7;
	
	// Number of fixed System entries, on top of the list
	private final static int FIXED_SYSTEM_COUNT = 4; // CPU, memory, uptime and loadavg
	
	private float cpu = 0;
	private long memoryUsed = 0;
	private long memoryTotal = 1;
	private long uptime = 0; 
	private float[] loadAvg; 
	
	private List<Disk> disks;
	private List<Process> processes;
	private List<String[]> customCommands;
	
	public SystemAdapter(Context context) {
		super();
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		loadAvg = new float[] { 0f, 0f, 0f };
		disks = new ArrayList<Disk>();
		processes = new ArrayList<Process>();
		customCommands = new ArrayList<String[]>();
	}
	
	@Override public View getView(int position, View convertView, ViewGroup parent) {
		
		// Inflate layout, hide non-used views on complex list item
		int type = getItemViewType(position);
		if (convertView == null) {
			convertView = inflater.inflate(type == TYPE_SEPARATOR ? R.layout.list_item_separator : R.layout.list_item_complex, null);
			if (type != TYPE_SEPARATOR) convertView.findViewById(R.id.list_item_icon).setVisibility(View.GONE);
			switch (type) {
				case TYPE_CPU:
					convertView.findViewById(R.id.list_item_description).setVisibility(View.GONE);
					((TextView)convertView.findViewById(R.id.list_item_text)).setTypeface(null, Typeface.BOLD);
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("CPU usage");
					break;
				case TYPE_MEMORY:
					((TextView)convertView.findViewById(R.id.list_item_text)).setTypeface(null, Typeface.BOLD);
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("Memory usage");
					break;
				case TYPE_UPTIME:
					convertView.findViewById(R.id.list_item_description).setVisibility(View.GONE);
					((TextView)convertView.findViewById(R.id.list_item_text)).setTypeface(null, Typeface.BOLD);
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("Uptime");
					break;
				case TYPE_LOADAVG:
					convertView.findViewById(R.id.list_item_description).setVisibility(View.GONE);
					((TextView)convertView.findViewById(R.id.list_item_text)).setTypeface(null, Typeface.BOLD);
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("Load average");
					break;
				case TYPE_PROCESS:
					convertView.findViewById(R.id.list_item_info).setVisibility(View.GONE);
					break;
				case TYPE_CUSTOM_COMMAND:
					convertView.findViewById(R.id.list_item_description).setVisibility(View.GONE);
					((TextView)convertView.findViewById(R.id.list_item_text)).setTypeface(null, Typeface.BOLD);
					break;
			}
		}
		
		// Display data
		switch (type) {
			case TYPE_CPU:
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(String.format("%.0f %%", cpu));
				SystemAdapter.setProgress(convertView, cpu, 100);
				break;
			case TYPE_MEMORY:
				((TextView)convertView.findViewById(R.id.list_item_description)).setText("Used " + StringUtils.humanReadableByteCount(memoryUsed * 1024) + " of " + StringUtils.humanReadableByteCount(memoryTotal * 1024) + " total");
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(String.format("%.0f %%", ((float)memoryUsed / (float)memoryTotal) * 100));
				SystemAdapter.setProgress(convertView, memoryUsed, memoryTotal);
				break;
			case TYPE_UPTIME:
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(getHumanReadableUptime());
				break;
			case TYPE_LOADAVG:
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(String.format("%.2f / %.2f / %.2f", loadAvg[0], loadAvg[1], loadAvg[2]));
				break;
			case TYPE_SEPARATOR:
				if (position == 0) {
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("System");
				} else if (!disks.isEmpty() && ((position == FIXED_SYSTEM_COUNT + 1 && customCommands.isEmpty()) || (position == FIXED_SYSTEM_COUNT + 1 + customCommands.size()))) {
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("Disks");
				} else {
					((TextView)convertView.findViewById(R.id.list_item_text)).setText("Top processes");
				}
				break;
			case TYPE_DISK:
				Disk disk = (Disk)getItem(position);
				((TextView)convertView.findViewById(R.id.list_item_text)).setText(disk.getMountpoint() + " (" + disk.getType() + ")");
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(String.format("%.0f %%", disk.getUsagePercent()));
				((TextView)convertView.findViewById(R.id.list_item_description)).setText("Free " + disk.getHumanReadableFree() + " of " + disk.getHumanReadableTotal() + " total");
				SystemAdapter.setProgress(convertView, disk.getUsage());
				break;
			case TYPE_PROCESS:
				Process process = (Process)getItem(position);
				((TextView)convertView.findViewById(R.id.list_item_text)).setText(process.getCommand());
				((TextView)convertView.findViewById(R.id.list_item_description)).setText(String.format("CPU usage: %.0f %%, memory usage: %.0f %%", process.getCpuPercent(), process.getMemoryPercent()));
				SystemAdapter.setProgress(convertView, process.getCpuPercent(), 100);
				break;
			case TYPE_CUSTOM_COMMAND:
				String[] customCommand = (String[])getItem(position);
				((TextView)convertView.findViewById(R.id.list_item_text)).setText(customCommand[0]);
				((TextView)convertView.findViewById(R.id.list_item_info)).setText(customCommand[1]);
				break;
		}
		
		return convertView;
	}

	@Override public int getViewTypeCount() {
		return 8;
	}
	
	@Override public int getItemViewType(int position) {
		if (position == 1) return TYPE_CPU;
		if (position == 2) return TYPE_MEMORY;
		if (position == 3) return TYPE_UPTIME;
		if (position == 4) return TYPE_LOADAVG;

		// Custom commands section
		int from = FIXED_SYSTEM_COUNT;
		if (!customCommands.isEmpty() && position > from && position <= from + customCommands.size()) return TYPE_CUSTOM_COMMAND;
		from += customCommands.size();

		// Disks section
		if (!disks.isEmpty()) from++;
		if (!disks.isEmpty() && position > from && position <= from + disks.size()) return TYPE_DISK;
		from += disks.size();
		
		// Processes
		from++;
		if (position > from) return TYPE_PROCESS;
		
		return TYPE_SEPARATOR;
	}

	@Override public int getCount() {
		return 1 + FIXED_SYSTEM_COUNT + customCommands.size() + (disks.isEmpty() ? 0 : 1 + disks.size()) + 1 + processes.size();
	}

	@Override public Object getItem(int position) {
		switch (getItemViewType(position)) {
			case TYPE_CUSTOM_COMMAND: return customCommands.get(position - 1 - FIXED_SYSTEM_COUNT);
			case TYPE_DISK: return disks.get(position - 1 - FIXED_SYSTEM_COUNT - customCommands.size() - 1);
			case TYPE_PROCESS: return processes.get(position - 1 - FIXED_SYSTEM_COUNT - customCommands.size() - 1 - (disks.isEmpty() ? 0 : 1 + disks.size()));
			default: return null;
		}
	}

	@Override public long getItemId(int position) {
		return position;
	}
	
	public void setCpu(float cpu) {
		this.cpu = cpu;
	}
	
	public void setMemoryTotal(int memoryTotal) {
		this.memoryTotal = memoryTotal;
	}
	
	public void setMemoryUsed(int memoryUsed) {
		this.memoryUsed = memoryUsed;
	}
	
	public void setUptime(int uptime) {
		this.uptime = uptime;
	}

	public void setLoadAvg(float load1, float load5, float load15) {
		loadAvg[0] = load1;
		loadAvg[1] = load5;
		loadAvg[2] = load15;
	}
	
	public void addDisk(Disk disk) {
		disks.add(disk);
	}
	
	public void addProcess(Process process) {
		processes.add(process);
	}

	public void addCustomCommand(String label, String output) {
		customCommands.add(new String[] { label, output });
	}
	
	public void clearAll() {
		disks.clear();
		processes.clear();
		customCommands.clear();
		notifyDataSetChanged();
	}

	@SuppressLint("DefaultLocale") public String getHumanReadableUptime() {
		if (uptime < 60) return String.format("%d seconds", uptime);
		if (uptime < 60 * 60) return String.format("%d minutes %d seconds", uptime / 60, uptime % 60);
		if (uptime < 60 * 60 * 24) return String.format("%d hours %d minutes", uptime / (60 * 60), (uptime / 60) % 60);
		return String.format("%d days %d hours", uptime / (60 * 60 * 24), (uptime / (60 * 60)) % 24);
	}

	public static void setProgress(View view, float value) {
		if (value < 0) value = 0;
		if (value > 1) value = 1;
		
		// If API < 11, layout is different, 100% is 0%
		if (android.os.Build.VERSION.SDK_INT < 11 && value == 1) value = 0;
		
		View progress = view.findViewById(R.id.list_item_progress);
		View progress_inverse = view.findViewById(R.id.list_item_progress_inverse);
		progress.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, value));
		progress_inverse.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f - value));
	}

	public static void setProgress(View view, float value, float max) {
		setProgress(view, value / max);
	}

}
