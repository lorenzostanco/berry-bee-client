package com.lorenzostanco.berrybeeclient.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

public class Folder {
	
	private String name;
	private long mtime;
	
	public Folder(String name, long mtime) {
		this.name = name;
		this.mtime = mtime;
	}
	
	public Folder(JSONObject o) throws JSONException {
		this(o.getString("name"), o.getLong("mtime"));
	}

	public String getName() {
		return name;
	}
	
	public long getModificationTime() {
		return mtime;
	}
	
	public String getHumanReadableModificationTime() {
		Date date = new Date(mtime * 1000);
		return new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(date);
	}
	
}
