package com.lorenzostanco.berrybeeclient;

import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.lorenzostanco.berrybeeclient.view.ServicesListActivity;
import com.lorenzostanco.utils.JSONWebService;

/**
 * Base abstract activity for all other Berry bee activities.
 * Service-specific activities will extends this class.
 */
public abstract class BerryBeeActivity extends ActionBarActivity {
	
	public static final String TAG = "BerryBee";
	
	private String service;
	private String serviceTitle;
	private BerryBeeWebService ws;

	private boolean autoRefresh = false;
	private int autoRefreshSeconds = 10;
	private Timer autoRefreshTimer;

	private Menu menu = null;
	private boolean refreshActionButtonStateRefreshing = false;

	private String lastErrorDialog = null;
	
	/** Setup web service and error displaying */
	@SuppressLint("NewApi") @Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (!(this instanceof ServicesListActivity)) overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left_partial);
		
		// Create JSON web service
		SharedPreferences preferences = getPreferences();
		String baseURL = preferences.getString("baseURL", "");
		String token = preferences.getString("token", "");
		ws = new BerryBeeWebService(baseURL, token);
		
		// Search service info in the Intent
		Intent intent = getIntent();
		service = intent.getStringExtra("service");
		serviceTitle = intent.getStringExtra("serviceTitle");
		if (serviceTitle != null) setTitle(serviceTitle);
		
		// Action bar
		if (getSupportActionBar() != null) getSupportActionBar().setElevation(getResources().getDimension(R.dimen.actionbar_elevation));
		
		// Alert on errors
		ws.addOnErrorListener(new JSONWebService.OnErrorListener() {
			@Override public void onError(String url, String type, String message) {
				Log.e(TAG, "[WS] " + type + ": " + url);
				if (message == null || message.equals("")) message = "Unknown error.";
				if (type.equals("MalformedURLException")) message = getString(R.string.error_url_malformed);
				if (type.equals("UnknownHostException")) message = getString(isNetworkAvailable() ? R.string.error_unknown_host : R.string.error_network_unavailable);
				if (type.equals("JSONException")) message = getString(R.string.error_invalid_json);
				if (type.equals("FileNotFoundException")) message = getString(R.string.error_not_found);
				showErrorDialog(message);
			}
		});

		// Manage auto refresh
		ws.addOnCompleteListener(new JSONWebService.OnCompleteListener() {
			@Override public void onComplete(String url) {
				
				// Empty scheduled tasks queue (but should be empty in normal circumstances)
				suspendAutoRefresh();
				
				// Schedule a service request
				if (autoRefresh) {
					autoRefreshTimer = new Timer();
					autoRefreshTimer.schedule(new TimerTask() {
						@Override public void run() {
							getBerryBeeActivity().runOnUiThread(new Runnable() {
								@Override public void run() {
									refresh();
								}
							});
						}
					}, autoRefreshSeconds * 1000);
					Log.i(TAG, "[AR] Auto refreshing in " + autoRefreshSeconds + ".");	
				}
				
			}
		});
		
	}
	
	/** Sets content view and setup default WS events */
	@SuppressLint("NewApi") public void setContentView(int layoutResID) {
		
		// Set content view
		super.setContentView(layoutResID);
		if (!(this instanceof ServicesListActivity)) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		// If activity has an "empty" text...
		final TextView emptyText = (TextView)findViewById(R.id.empty_text);
		final boolean hasEmptyText = (emptyText != null);
		if (hasEmptyText) emptyText.setTag(emptyText.getText().toString());
		
		// Display indeterminate progress icon in the activity title
		setRefreshActionButtonState(false);
		ws.addOnRequestListener(new JSONWebService.OnRequestListener() {
			@Override public void onRequest(String url) {
				Log.d(TAG, "[WS] Request: " + url);
				setRefreshActionButtonState(true);
				if (hasEmptyText) emptyText.setText(R.string.loading);
			}
		});
		ws.addOnCancelListener(new JSONWebService.OnCancelListener() {
			@Override public void onCancel(String url) {
				Log.d(TAG, "[WS] Cancel: " + url);
				setRefreshActionButtonState(false);
				if (hasEmptyText) emptyText.setText(emptyText.getTag().toString());
			}
		});
		ws.addOnCompleteListener(new JSONWebService.OnCompleteListener() {
			@Override public void onComplete(String url) {
				Log.i(TAG, "[WS] Complete: " + url);
				setRefreshActionButtonState(false);
				if (hasEmptyText) emptyText.setText(emptyText.getTag().toString());
			}
		});
		
	}
	
	/** Returns activity itself */
	public BerryBeeActivity getBerryBeeActivity() {
		return this;
	}
	
	/** Returns Berry bee web service */
	public BerryBeeWebService getBerryBeeWebService() {
		return ws;
	}
	
	/** Returns the service name, or NULL */
	public String getServiceName() {
		return service;
	}
	
	/** Returns the service title, or NULL */
	public String getServiceTitle() {
		return serviceTitle;
	}
	
	/** Starts a request for current service name */
	public void refresh() {
		ws.requestService(service);
	}

	/** Enable/disable auto-refresh */
	public void setAutoRefresh(boolean enable) {
		setAutoRefresh(enable, null);
	}

	/** Enable/disable auto-refresh, sets menu item check status */
	@SuppressLint("NewApi") public void setAutoRefresh(boolean enable, MenuItem item) {
		autoRefresh = enable;
		Log.i(TAG, "[AR] Auto refresh " + (enable ? "enabled" : "disabled") + ".");
		if (item != null) item.setChecked(enable);
		if (android.os.Build.VERSION.SDK_INT >= 11) invalidateOptionsMenu();
		if (enable == false) {
			suspendAutoRefresh();
			Toast.makeText(this, R.string.auto_refresh_disabled, Toast.LENGTH_SHORT).show();
		} else {
			setAutoRefreshInterval(autoRefreshSeconds);
			Toast.makeText(this, String.format(getString(R.string.auto_refresh_enabled), autoRefreshSeconds), Toast.LENGTH_SHORT).show();
			refresh();
		}
	}

	/** Sets up auto-refresh every x seconds */
	public void setAutoRefreshInterval(int seconds) {
		autoRefreshSeconds = seconds;
		Log.i(TAG, "[AR] Auto refresh set to " + seconds + " seconds.");
	}

	/** Suspends auto refresh until next event (useful to avoid commands interruptions) */
	public void suspendAutoRefresh() {
		
		// Empty scheduled tasks queue
		if (autoRefreshTimer != null) {
			autoRefreshTimer.cancel();
			autoRefreshTimer.purge();
			autoRefreshTimer = null;
			Log.i(TAG, "[AR] Auto refresh suspended.");
		} else {
			Log.d(TAG, "[AR] Auto refresh suspended, but nothing to suspend.");
		}
		
	}

	public SharedPreferences getPreferences() {
		return getSharedPreferences(TAG, MODE_PRIVATE);
	}
	
	/** Displays an error dialog */
	public void showErrorDialog(String message) {
		showErrorDialog(message, null, null);
	}
	
	/** Displays an error dialog */
	@SuppressLint("NewApi") public void showErrorDialog(String message, String extraButton, DialogInterface.OnClickListener extraButtonClickListener) {
		
		// Avoid identical multiple dialogs
		if (lastErrorDialog != null && (message + extraButton).equals(lastErrorDialog)) return;
		
		// Build dialog
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.error).setMessage(message);
		builder.setPositiveButton(android.R.string.ok, null);
		if (extraButton != null) builder.setNeutralButton(extraButton, extraButtonClickListener);
		
		// Setup icon
		if (android.os.Build.VERSION.SDK_INT >= 11) builder.setIconAttribute(android.R.attr.alertDialogIcon);
		else builder.setIcon(android.R.drawable.ic_dialog_alert);
		
		// Build alert dialog
		AlertDialog dialog = builder.create();
		
		// Remember this alert message as last
		lastErrorDialog = message + extraButton;
		dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
			@Override public void onDismiss(DialogInterface dialog) {
				Log.w(TAG, "Error dialog dismissed.");
				lastErrorDialog = null;
			}
		});
		
		// Show
		dialog.show();
		
	}

	/** Starts default request on resume */
	@Override protected void onResume() {
		super.onResume();
		Log.d(TAG, "[LC] Activity resumed, starting default service request.");
		refresh();
	}

	/** Calls super method only, do not starts default request on resume */
	protected void callSuperOnResume() {
		Log.d(TAG, "[LC] Activity resumed, NOT starting default service request.");
		super.onResume();
	}
	
	public Menu getMenu() {
		return menu;
	}

	/** Shows/hides progress icon on the action bar */
	public void setRefreshActionButtonState(final boolean refreshing) {
	    if (menu != null) {
	        final MenuItem refreshItem = menu.findItem(R.id.action_refresh);
	        if (refreshItem != null) {
	        	if (refreshing) {
	        		MenuItemCompat.setActionView(refreshItem, R.layout.actionbar_indeterminate_progress);
	        	} else {
	        		MenuItemCompat.setActionView(refreshItem, null);
	        	}
	        }
	    }
	    
	    // Keep state if menu is yet NULL
	    refreshActionButtonStateRefreshing = refreshing;
	    
	}

	/** Suspends auto refresh on pause */
	@Override protected void onPause() {
		super.onPause();
		Log.d(TAG, "[LC] Activity paused, suspending auto refresh.");
		suspendAutoRefresh();
	}
	
	@Override public boolean onCreateOptionsMenu(Menu menu) {
		this.menu = menu;
		boolean ret = super.onCreateOptionsMenu(menu);
		MenuItem refreshMenuItem = menu.findItem(R.id.action_refresh);
		if (refreshMenuItem != null) setRefreshActionButtonState(refreshActionButtonStateRefreshing);
		MenuItem autoRefreshItem = menu.findItem(R.id.action_auto_refresh);
		if (autoRefreshItem != null) autoRefreshItem.setChecked(autoRefresh);
		return ret;
	}
	
	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		MenuItem autoRefreshItem = menu.findItem(R.id.action_auto_refresh);
		if (autoRefreshItem != null) autoRefreshItem.setChecked(autoRefresh);
		return true;
	}
	
	/** Let the home icon close the activity, manage "refresh" and "auto refresh" items */
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			case R.id.action_refresh:
				refresh();
				return true;
			case R.id.action_auto_refresh:
				setAutoRefresh(!item.isChecked(), item);
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override public void finish() {
		super.finish();
		if (!(this instanceof ServicesListActivity)) overridePendingTransition(R.anim.slide_in_left_partial, R.anim.slide_out_right);
	}

	@Override protected void onDestroy() {
		super.onDestroy();
		if (!(this instanceof ServicesListActivity)) overridePendingTransition(R.anim.slide_in_left_partial, R.anim.slide_out_right);
	}

	public boolean isNetworkAvailable() {
		return isNetworkAvailable(this);
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
}

