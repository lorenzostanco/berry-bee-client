package com.lorenzostanco.utils;

import java.text.DecimalFormat;
import java.util.Locale;

public class StringUtils {

	public static String humanReadableByteCount(long bytes) {
		return humanReadableByteCount(bytes, 0);
	}

	public static String humanReadableByteCount(long bytes, int decimals) {
		if (bytes < 1024) return bytes + " B"; 
		int exp = (int) (Math.log(bytes) / Math.log(1024));
		String pre = "" + ("KMGTPE").charAt(exp - 1);
		return String.format("%." + decimals + "f %sB", bytes / Math.pow(1024, exp), pre); 
	}

	public static String humanReadableByteCount(long bytes, String decimalFormatPattern) {
		if (bytes < 1024) return bytes + " B"; 
		int exp = (int) (Math.log(bytes) / Math.log(1024));
		String pre = "" + ("KMGTPE").charAt(exp - 1);
		return new DecimalFormat(decimalFormatPattern).format(bytes / Math.pow(1024, exp)) + " " + pre + "B"; 
	}

	public static String normalizeMimeType(String type) {
		if (type == null) return null;
		type = type.trim().toLowerCase(Locale.US);
		final int semicolonIndex = type.indexOf(';');
		if (semicolonIndex != -1) type = type.substring(0, semicolonIndex);
		return type;
	}

}
