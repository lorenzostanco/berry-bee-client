package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.lorenzostanco.berrybeeclient.BerryBeeActivity;
import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.adapter.ServiceListAdapter;
import com.lorenzostanco.berrybeeclient.model.Service;
import com.lorenzostanco.utils.JSONWebService;

public class ServicesListActivity extends BerryBeeActivity {

	private boolean doingAuthentication = false;
	
	@SuppressLint("NewApi") @Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_services_list);
		
		showEmpty(true);
		
		// Set title
		try {
			String version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
			setTitle(getString(R.string.launcher_name) + " " + version);
		} catch (NameNotFoundException e) { }
		
		// Hide home icon, because this is the main activity
		if (android.os.Build.VERSION.SDK_INT >= 11) {
			getSupportActionBar().setDisplayShowHomeEnabled(true);
			getSupportActionBar().setHomeButtonEnabled(false);
			getSupportActionBar().setIcon(R.drawable.ic_launcher);
			setTitle("  " + getTitle());
		}
		
		// Was authenticating?
		if (savedInstanceState != null && savedInstanceState.getBoolean("doingAuthentication", false)) {
			Log.i(TAG, "[LC] Created, was killed while doing authentication.");
			doingAuthentication = true;
		}

		// Setup services list
		final ServiceListAdapter adapter = new ServiceListAdapter(this);
		ListView list = (ListView)findViewById(R.id.list);
		list.setAdapter(adapter);
		
		// On data received, display services list
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {
			@Override public void onSuccess(String url, JSONObject response) {
				adapter.clear();
				try {
					adapter.setTitle(response.getString("name"));
					adapter.setDescription(response.getString("description"));
					JSONArray services = response.getJSONArray("services");
					for (int i = 0; i < services.length(); i++) adapter.add(new Service(services.getJSONObject(i)));
					
					// Save services list in preferences
					getPreferences().edit().putString("services", services.toString()).commit();
					
				} catch (JSONException x) {
					showErrorDialog(getString(R.string.error_response_unexpected));
				}
				adapter.notifyDataSetChanged();
				showEmpty(adapter.isEmpty());
			}
		});
		
		// On errors, show the empty message
		getBerryBeeWebService().addOnErrorListener(new JSONWebService.OnErrorListener() {
			@Override public void onError(String url, String type, String message) {
				showEmpty(true);
			}
		});
		
		// On item click, launch activity for clicked service
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Service service = adapter.getItem(position);
				if (service == null) return;
				Intent intent = service.getStartActivityIntent(getBerryBeeActivity());
				if (intent == null) {
					showErrorDialog(getString(R.string.error_service_not_implemented));
				} else {
					startActivity(intent);
				}
			}
		});
		
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.services_list, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_auth:
				startAuthentication();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	public void startAuthentication() {
		doingAuthentication = true;
		startActivity(new Intent(this, AuthActivity.class));
	}

	public void showEmpty(boolean empty) {
		findViewById(R.id.list_wrapper).setVisibility(empty ? View.GONE : View.VISIBLE);
		findViewById(R.id.empty).setVisibility(empty ? View.VISIBLE : View.GONE);
	}
	
	@Override protected void onResume() {
		
		// Resumed after authentication activity?
		if (doingAuthentication) {
			Log.d(TAG, "[LC] Resumed after authentication.");
			doingAuthentication = false;
			
			// Do not call super.OnResume (bacase default request will be started),
			// instead call OnResume on super-super (Activity.OnResume)
			super.callSuperOnResume();
			
			// Setup new web service parameters
			SharedPreferences preferences = getPreferences();
			String baseURL = preferences.getString("baseURL", "");
			if (baseURL.equals("")) { finish(); return; }
			getBerryBeeWebService().setBaseURL(baseURL);
			getBerryBeeWebService().setToken(preferences.getString("token", ""));
			
			// Restart services list request
			refresh();
			
		// Normal resume?
		} else {

			// First time running Berry bee client? Go to authentication.
			if (getPreferences().getString("baseURL", "").equals("")) {
				startAuthentication();
				super.callSuperOnResume();
			} else {
				super.onResume();
			}
			
		}
		
	}

	@Override protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("doingAuthentication", doingAuthentication);
	}
	
}
