package com.lorenzostanco.berrybeeclient.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.model.Service;

public class ServiceListAdapter extends BaseAdapter {
	
	private List<Service> services;
	private String title = "";
	private String description = "";
	
	private LayoutInflater inflater;
	
	public ServiceListAdapter(Context context) {
		super();
		services = new ArrayList<Service>();
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override public View getView(int position, View convertView, ViewGroup parent) {
		if (position == 0) {
			if (convertView == null) convertView = inflater.inflate(R.layout.list_item_services_header, parent, false);
			((TextView)convertView.findViewById(R.id.text_title)).setText(this.title);
			((TextView)convertView.findViewById(R.id.text_description)).setText(this.description);
		} else {
			Service service = getItem(position);
			if (convertView == null) convertView = inflater.inflate(R.layout.list_item_service, parent, false);
			((TextView)convertView.findViewById(R.id.list_item_text)).setText(service.getTitle());
			((TextView)convertView.findViewById(R.id.list_item_description)).setText(service.getDescription());
			((TextView)convertView.findViewById(R.id.list_item_description)).setVisibility(service.getDescription() == null ? View.GONE : View.VISIBLE);
			((ImageView)convertView.findViewById(R.id.list_item_icon)).setImageResource(service.getIconResource());
		}
		return convertView;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void add(Service service) {
		this.services.add(service);
	}

	public void clear() {
		this.services.clear();
	}
	
	@Override public int getItemViewType(int position) {
		return position == 0 ? 0 : 1;
	}

	@Override public int getViewTypeCount() {
		return 2;
	}
	
	@Override public int getCount() {
		return this.services.size() + 1;
	}

	@Override public Service getItem(int position) {
		if (position == 0) return null;
		return this.services.get(position - 1);
	}

	@Override public long getItemId(int position) {
		return position;
	}
	
	@Override public boolean isEnabled(int position) {
		return position > 0;
	}
	
}
