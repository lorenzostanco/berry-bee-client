package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.model.Disk;
import com.lorenzostanco.utils.JSONWebService;

public class MountActivity extends LsActivity {
	
	private Menu menu;
	private boolean mounted = false;
	private boolean mountable = false;
	private boolean umountable = false;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.callSuperOnCreate(savedInstanceState);
		setContentView(R.layout.activity_mount);

		// Do not show error on Ls parsing, maybe because device is not mounted...
		showLsParseError = false;
		
		// Manage mounting stuffs
		setMounted(false);
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {
			@Override public void onSuccess(String url, JSONObject response) {

				// Set mounting status
				mountable = response.optBoolean("mountable", false);
				umountable = response.optBoolean("umountable", false);
				setMounted(response.optBoolean("mounted", false)); // This refreshes menu
				
				// Get disk status if mounted
				if (isMounted()) try {
					
					Disk disk = new Disk(response.getJSONObject("disk"));
					findViewById(R.id.list_item_icon).setVisibility(View.GONE);
					findViewById(R.id.list_item_description).setVisibility(View.GONE);
					((TextView)findViewById(R.id.list_item_text)).setTypeface(null, Typeface.BOLD);
					((TextView)findViewById(R.id.list_item_text)).setText("Free " + disk.getHumanReadableFree() + " of " + disk.getHumanReadableTotal() + " (" + disk.getType() + ")");
					((TextView)findViewById(R.id.list_item_info)).setText(String.format("%.0f %%", disk.getUsagePercent()));
					View progress = findViewById(R.id.list_item_progress);
					View progress_inverse = findViewById(R.id.list_item_progress_inverse);
					progress.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, disk.getUsage()));
					progress_inverse.setLayoutParams(new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, 1f - disk.getUsage()));
					
				} catch (JSONException x) {
					showErrorDialog(getString(R.string.error_response_unexpected));
				}
				
			}
		});

		// Setup standard Ls stuff
		setupLsList();
		
	}
	
	public void mount() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "mount");
	}
	
	public void umount() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "umount");
	}
	
	public void setMounted(boolean mounted) {
		this.mounted = mounted;
		
		// Empty button and text
		((TextView)findViewById(R.id.empty_text)).setText(isMounted() ? R.string.empty_ls : R.string.empty_mount);
		
		// Disk indicator
		findViewById(R.id.disk_wrapper).setVisibility(isMounted() ? View.VISIBLE : View.GONE);
		
		// Refresh menu
		if (this.menu != null) onPrepareOptionsMenu(this.menu);
		
	}
	
	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_mount).setVisible(!isMounted() && mountable);
		menu.findItem(R.id.action_umount).setVisible(isMounted() && umountable);
		MenuItem mkdirItem = menu.findItem(R.id.action_mkdir);
		if (mkdirItem != null) mkdirItem.setVisible(isMounted() && canMkdir);
		return true;
	}
	
	public boolean isMounted() {
		return mounted;
	}

	protected boolean canCopy() {
		return isMounted();
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.mount, menu);
		this.menu = menu;
		super.callSuperOnCreateOptionsMenu(menu);
		return true;
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_mount:
				mount();
				return true;
			case R.id.action_umount:
				umount();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
}
