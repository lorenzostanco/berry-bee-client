package com.lorenzostanco.berrybeeclient.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import com.lorenzostanco.utils.StringUtils;

public class File {
	
	private String name;
	private long size;
	private long mtime;
	private long copying = 0;
	private String mimeType;

	public File(String name, long size, long mtime, long copying, String mimeType) {
		this.name = name;
		this.size = size;
		this.mtime = mtime;
		this.copying = copying;
		this.mimeType = mimeType;
	}

	public File(JSONObject o) throws JSONException {
		this(o.getString("name"), Long.parseLong(o.getString("size")), o.getLong("mtime"), Long.parseLong(o.getString("copying")), o.getString("mime"));
	}
	
	public String getName() {
		return name;
	}
	
	public long getSize() {
		return size;
	}
	
	public String getHumanReadableSize() {
		return StringUtils.humanReadableByteCount(size, "@@#");
	}
	
	public long getModificationTime() {
		return mtime;
	}
	
	public String getHumanReadableModificationTime() {
		Date date = new Date(mtime * 1000);
		return new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(date);
	}
	
	public long getCopying() {
		return copying;
	}

	public String getHumanReadableCopying() {
		return StringUtils.humanReadableByteCount(copying, "@@#");
	}
	
	public float getCopyingProgress() {
		return (float)size / (float)copying;
	}
	
	public float getCopyingProgressPercent() {
		return getCopyingProgress() * 100;
	}
	
	public boolean isCopying() {
		return copying > 0;
	}
	
	public String getMimeType() {
		return mimeType;
	}
	
}
