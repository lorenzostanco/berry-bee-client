package com.lorenzostanco.berrybeeclient.model;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.view.LogActivity;
import com.lorenzostanco.berrybeeclient.view.LsActivity;
import com.lorenzostanco.berrybeeclient.view.MountActivity;
import com.lorenzostanco.berrybeeclient.view.PushButtonsPanelActivity;
import com.lorenzostanco.berrybeeclient.view.SystemActivity;
import com.lorenzostanco.berrybeeclient.view.TransmissionActivity;
import com.lorenzostanco.berrybeeclient.view.XBMCActivity;
import com.lorenzostanco.berrybeeclient.view.aMuleActivity;

public class Service {
	
	private String name;
	private String title;
	private String description;
	private String type;

	public Service(String name, String title, String description, String type) {
		this.name = name;
		this.title = title;
		this.description = description;
		this.type = type;
	}

	public Service(JSONObject o) throws JSONException {
		this(o.getString("name"), o.getString("title"), o.isNull("description") ? null : o.getString("description"), o.getString("class"));
	}

	public String getName() {
		return name;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String getType() {
		return type;
	}

	public int getIconResource() {
		if (type.equals("System")) return R.drawable.ic_insert_chart_black_48dp;
		if (type.equals("Ls")) return R.drawable.ic_folder_black_48dp;
		if (type.equals("aMule")) return R.drawable.ic_file_download_black_48dp;
		if (type.equals("Transmission")) return R.drawable.ic_file_download_black_48dp;
		if (type.equals("XBMC")) return R.drawable.ic_tv_black_48dp;
		if (type.equals("Mount")) return R.drawable.ic_usb_black_48dp;
		if (type.equals("Log")) return R.drawable.ic_view_list_black_48dp;
		if (type.equals("PushButtonsPanel")) return R.drawable.ic_dashboard_black_48dp;
		return R.drawable.ic_error_black_48dp;
	}

	public Intent getStartActivityIntent(Context context) {
		Intent intent;
		if (type.equals("Log")) intent = new Intent(context, LogActivity.class);
		else if (type.equals("System")) intent = new Intent(context, SystemActivity.class);
		else if (type.equals("Ls")) intent = new Intent(context, LsActivity.class);
		else if (type.equals("aMule")) intent = new Intent(context, aMuleActivity.class);
		else if (type.equals("Transmission")) intent = new Intent(context, TransmissionActivity.class);
		else if (type.equals("XBMC")) intent = new Intent(context, XBMCActivity.class);
		else if (type.equals("Mount")) intent = new Intent(context, MountActivity.class);
		else if (type.equals("PushButtonsPanel")) intent = new Intent(context, PushButtonsPanelActivity.class);
		else return null; 
		intent.putExtra("service", name);
		intent.putExtra("serviceTitle", title);
		return intent;
	}
	
}
