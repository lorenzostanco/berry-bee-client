package com.lorenzostanco.berrybeeclient.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.lorenzostanco.berrybeeclient.BerryBeeActivity;
import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.berrybeeclient.adapter.LsAdapter;
import com.lorenzostanco.berrybeeclient.model.File;
import com.lorenzostanco.berrybeeclient.model.Folder;
import com.lorenzostanco.berrybeeclient.model.Service;
import com.lorenzostanco.utils.JSONWebService;
import com.lorenzostanco.utils.StringUtils;

public class LsActivity extends BerryBeeActivity {

	// Folder name
	protected String folder;
	
	// Available actions
	protected boolean canDownload = false;
	protected boolean canRm = false;
	protected boolean canMove = false;
	protected boolean canRename = false;
	protected boolean canMkdir = false;
	protected boolean canBrowse = false;
	protected List<Service> copyTo = null;
	protected boolean copied = false;
	
	// Browsing stuff (a list of nested subdirectories names)
	protected List<String> browsing;
	
	// If I should hide list on next errors
	protected boolean hideListOnNextError = true;
	
	// Show errors if cannot parse Ls response?
	protected boolean showLsParseError = true;
	
	@Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ls);	

		setupLsList();

	}

	/** Calls super method only, do not setup content view */
	protected void callSuperOnCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public void setupLsList() {

		showEmpty(true);
		
		// Setup browsing
		browsing = new ArrayList<String>();

		// Setup list
		final ListView list = (ListView)findViewById(R.id.list);
		final LsAdapter adapter = new LsAdapter(this);
		list.setAdapter(adapter);
		
		// On errors, show the empty message
		getBerryBeeWebService().addOnErrorListener(new JSONWebService.OnErrorListener() {
			@Override public void onError(String url, String type, String message) {
				if (hideListOnNextError) showEmpty(true);
				hideListOnNextError = true;
			}
		});
		
		// On success display files list
		copyTo = new ArrayList<Service>();
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {	
			@Override public void onSuccess(String url, JSONObject response) {
				adapter.clearAll();
				try {
					
					// Folder name
					folder = response.getString("folder");
					
					// Available actions
					canDownload = response.optBoolean("download", false);
					canRm = response.optBoolean("rm", false);
					canMove = response.optBoolean("move", false);
					canRename = response.optBoolean("rename", false);
					canMkdir = response.optBoolean("mkdir", false);
					canBrowse = response.optBoolean("browse", false);
					
					// Refresh menu
					if (getMenu() != null) onPrepareOptionsMenu(getMenu());
					
					// Populate copy-to services array
					if (copyTo == null) copyTo = new ArrayList<Service>(); 
					copyTo.clear();
					JSONObject jsonCopyTo = new JSONObject("{}");
					try { jsonCopyTo = response.getJSONObject("copyto"); } catch (JSONException x) { }
					JSONArray jsonServices = new JSONArray(getPreferences().getString("services", "[]"));
					for (int i = 0; i < jsonServices.length(); i++) {
						if (jsonCopyTo.has(jsonServices.getJSONObject(i).getString("name"))) {
							copyTo.add(new Service(jsonServices.getJSONObject(i)));
						}
					}
					
				} catch (JSONException x) {
					showErrorDialog(getString(R.string.error_response_unexpected));
				}
				
				try {
					
					// If browsing, add a ".." folder
					if (isBrowsing()) adapter.addFolder(new Folder("..", 0));
					
					// Folders
					JSONArray folders = response.getJSONArray("subs");
					for (int i = 0; i < folders.length(); i++) adapter.addFolder(new Folder(folders.getJSONObject(i)));

					// Files
					JSONArray files = response.getJSONArray("files");
					for (int i = 0; i < files.length(); i++) adapter.addFile(new File(files.getJSONObject(i)));
					
					// All it's going fine, let's copy files if requested
					if (canCopy()) new Timer().schedule(new TimerTask() {
						@Override public void run() {
							runOnUiThread(new Runnable() {
								@Override public void run() {
									doCopy();
								}
							});
						}
					}, 500);
					
				} catch (JSONException x) {
					if (showLsParseError) {
						showErrorDialog(getString(R.string.error_response_unexpected));
					}
				}
				adapter.notifyDataSetChanged();
				showEmpty(adapter.isEmpty());
			}
		});
		
		// Items click
		final AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				// Is a directory? Handle browsing
				if (adapter.getItemViewType(position) == LsAdapter.TYPE_FOLDER) {
					if (!canBrowse) {
						Toast.makeText(getBerryBeeActivity(), R.string.action_browse_forbidden, Toast.LENGTH_LONG).show();
					} else {
						browse(((Folder)adapter.getItem(position)).getName());
						suspendAutoRefresh();
						refresh();
					}
				
				// Else, is a file
				} else {
					
					// Get file and create dialog
					final File file = (File)adapter.getItem(position);
					AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
					builder.setTitle(file.getName());
					
					// Create actions
					final List<String> actions = new ArrayList<String>();
					if (canDownload) actions.add(getString(R.string.action_file_open));
					if (canDownload) actions.add(getString(R.string.action_file_download));
					if (canDownload) actions.add(getString(R.string.action_file_share_url));
					if (canRename) actions.add(getString(R.string.action_file_rename));
					if (canMove && adapter.getFoldersCount() > 0) actions.add(getString(R.string.action_file_move));
					if (!copyTo.isEmpty()) actions.add(getString(R.string.action_file_copyto));
					if (canRm) actions.add(getString(R.string.action_file_rm));
					if (actions.isEmpty()) return;
					builder.setItems(actions.toArray(new String[]{}), new DialogInterface.OnClickListener() {
						@SuppressLint("NewApi") @Override public void onClick(DialogInterface dialog, int which) {
	
							// Open?
							if (actions.get(which).equals(getString(R.string.action_file_open))) {
								Map<String, String> params = new HashMap<String, String>();
								params.put("filename", file.getName());
								String url = getBerryBeeWebService().getRequestServiceUrl(getServiceName(), "download", makeBrowseParam(params));
								Log.i(TAG, "Opening URL: " + url);
								try {
									Intent intent = new Intent(Intent.ACTION_VIEW);
									intent.setDataAndType(Uri.parse(url), StringUtils.normalizeMimeType(file.getMimeType()));
									startActivity(intent);
								} catch (ActivityNotFoundException x) {
									Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
									startActivity(intent);
								}
							}
	
							// Download?
							if (actions.get(which).equals(getString(R.string.action_file_download))) {
								Map<String, String> params = new HashMap<String, String>();
								params.put("filename", file.getName());
								String url = getBerryBeeWebService().getRequestServiceUrl(getServiceName(), "download", makeBrowseParam(params));
								DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
								if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
								    request.allowScanningByMediaScanner();
								    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
								}
								request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, file.getName());
								request.setTitle(file.getName());
								request.setDescription(getServiceTitle() != null ? String.format(getString(R.string.action_file_download_description_title), getServiceTitle()) : getString(R.string.action_file_download_description));
								DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
								manager.enqueue(request);
							}

							// Share URL?
							if (actions.get(which).equals(getString(R.string.action_file_share_url))) {
								Map<String, String> params = new HashMap<String, String>();
								params.put("filename", file.getName());
								String url = getBerryBeeWebService().getRequestServiceUrl(getServiceName(), "download", makeBrowseParam(params));
								Log.i(TAG, "Sharing URL: " + url);
								Intent intent = new Intent(Intent.ACTION_SEND);
								intent.putExtra(Intent.EXTRA_TEXT, url);
								intent.setType("text/plain");
								startActivity(Intent.createChooser(intent, String.format(getString(R.string.action_file_share_url_to), file.getName())));
							}
							
							// Copy?
							if (actions.get(which).equals(getString(R.string.action_file_copyto))) {
	
								// Pick services
								List<String> serviceTitles = new ArrayList<String>();
								for (Service service : copyTo) serviceTitles.add(service.getTitle());
								AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
								builder.setTitle(R.string.action_file_copyto_select_service);
								builder.setItems(serviceTitles.toArray(new String[]{}), new DialogInterface.OnClickListener() {
									@Override public void onClick(DialogInterface dialog, int which) {
										Log.w(TAG, "Copying into: " + copyTo.get(which).getName());
										Intent intent = copyTo.get(which).getStartActivityIntent(getBerryBeeActivity());
										intent.putExtra("copyFolder", folder);
										if (isBrowsing()) intent.putExtra("copySourceBrowse", getBrowsePath());
										intent.putExtra("copyFilename", file.getName());
										startActivity(intent);
									}
								});
								builder.create().show();
								
							}
							
							// Move?
							if (actions.get(which).equals(getString(R.string.action_file_move))) {
	
								// Pick folders
								final List<Folder> folders = adapter.getFolders();
								List<String> folderNames = new ArrayList<String>();
								for (Folder folder : folders) folderNames.add(folder.getName());
								AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
								builder.setTitle(R.string.action_file_move_select_folder);
								builder.setItems(folderNames.toArray(new String[]{}), new DialogInterface.OnClickListener() {
									@Override public void onClick(DialogInterface dialog, int which) {
										suspendAutoRefresh();
										hideListOnNextError = false;
										Map<String, String> params = new HashMap<String, String>();
										params.put("filename", file.getName());
										params.put("sub", folders.get(which).getName());
										getBerryBeeWebService().requestService(getServiceName(), "move", makeBrowseParam(params));
									}
								});
								builder.create().show();
								
							}
							
							// Rename?
							if (actions.get(which).equals(getString(R.string.action_file_rename))) {
								
								// Prompt new filename
								AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
								builder.setTitle(R.string.action_file_rename_prompt);
								final View v = getLayoutInflater().inflate(R.layout.dialog_edit_text, null);
								final EditText input = (EditText)v.findViewById(R.id.edit_text);
								input.setText(file.getName());
								input.setSelection(0, file.getName().length());
								builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										suspendAutoRefresh();
										hideListOnNextError = false;
										Map<String, String> params = new HashMap<String, String>();
										params.put("old", file.getName());
										params.put("new", input.getText().toString());
										getBerryBeeWebService().requestService(getServiceName(), "rename", makeBrowseParam(params));
									}
								});
								builder.setNegativeButton(R.string.cancel, null);
								builder.setInverseBackgroundForced(true);
								builder.create();
								AlertDialog inputDialog = builder.create();
								inputDialog.setView(v);
								inputDialog.show();
								
							}
							
							// Remove?
							if (actions.get(which).equals(getString(R.string.action_file_rm))) {
								
								// Ask confirmation
								AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
								builder.setMessage(String.format(getString(R.string.action_file_rm_confirm), file.getName()));
								builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
									@Override public void onClick(DialogInterface dialog, int which) {
										suspendAutoRefresh();
										hideListOnNextError = false;
										Map<String, String> params = new HashMap<String, String>();
										params.put("filename", file.getName());
										getBerryBeeWebService().requestService(getServiceName(), "rm", makeBrowseParam(params));
									}
								});
								builder.setNegativeButton(R.string.no, null);
								builder.create().show();
								
							}
							
						}
					});
					
					// Show dialog
					builder.create().show();
				
				}
				
			}
		};
		list.setOnItemClickListener(itemClickListener);
		
		// Items long click
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

				// Is a directory? Handle actions
				if (adapter.getItemViewType(position) == LsAdapter.TYPE_FOLDER) {
					
					// Get folder and create dialog
					final Folder folder = (Folder)adapter.getItem(position);
					if (folder.getName() == "..") return false;
					AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
					builder.setTitle(folder.getName());
					
					// Create actions
					final List<String> actions = new ArrayList<String>();
					if (canMkdir) actions.add(getString(R.string.action_mkdir));
					if (canRename) actions.add(getString(R.string.action_file_rename));
					if (actions.isEmpty()) return false;
					builder.setItems(actions.toArray(new String[]{}), new DialogInterface.OnClickListener() {
						@SuppressLint("NewApi") @Override public void onClick(DialogInterface dialog, int which) {
	
							// Make directory
							if (actions.get(which).equals(getString(R.string.action_mkdir))) {
								doMkdir();
							}
							
							// Rename?
							if (actions.get(which).equals(getString(R.string.action_file_rename))) {
								
								// Prompt new filename
								AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
								builder.setTitle(R.string.action_file_rename_prompt_folder);
								final View v = getLayoutInflater().inflate(R.layout.dialog_edit_text, null);
								final EditText input = (EditText)v.findViewById(R.id.edit_text);
								input.setText(folder.getName());
								input.setSelection(0, folder.getName().length());
								builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										suspendAutoRefresh();
										hideListOnNextError = false;
										Map<String, String> params = new HashMap<String, String>();
										params.put("old", folder.getName());
										params.put("new", input.getText().toString());
										getBerryBeeWebService().requestService(getServiceName(), "rename", makeBrowseParam(params));
									}
								});
								builder.setNegativeButton(R.string.cancel, null);
								builder.setInverseBackgroundForced(true);
								AlertDialog inputDialog = builder.create();
								inputDialog.setView(v);
								inputDialog.show();

							}
							
						}
					});

					// Show dialog
					builder.create().show();
				
					// Click consumed
					return true;
				
				// Else, if it's a file, behave as normal click
				} else {
					itemClickListener.onItemClick(parent, view, position, id);
					return true;
				}
				
			}
		});
		
	}
	
	/** Override the refresh method, in order to persist browsing */
	public void refresh() {
		getBerryBeeWebService().requestService(getServiceName(), null, makeBrowseParam());
	}
	
	/** Browse a subdirectory, i.e. add the subdirectory name into the browse list.
	 * @param directory If "..", "../", "/.." or "/../", goes up one level, i.e. remove last subdirectory name from the brose list */
	protected void browse(String directory) {
		if (directory.equals("..") || directory.equals("../") || directory.equals("/..") || directory.equals("/../")) {
			browsing.remove(browsing.size() - 1);
		} else {
			browsing.add(directory);
		}
	}
	
	/** Returns TRUE if we are inside a subdirectory, i.e. if browsing list is not empty */
	protected boolean isBrowsing() {
		return !browsing.isEmpty();
	}
	
	/** Create a parameters map with browse parameter */ 
	protected Map<String, String> makeBrowseParam() {
		Map<String, String> params = new HashMap<String, String>();
		return makeBrowseParam(params);
	}

	/** Fill in an existent parameters map with browse parameter */ 
	protected Map<String, String> makeBrowseParam(Map<String, String> params) {
		if (canBrowse && isBrowsing()) params.put("browse", getBrowsePath());
		return params;
	}

	/** Get the browse path */ 
	protected String getBrowsePath() {
		String browse = "";
		for (String directory : browsing) browse += directory + "/";
		return browse;
	}
	
	protected boolean canCopy() {
		return true;
	}

	public void doCopy() {
		
		// Has to copy?
		boolean hasCopy = (getIntent().getStringExtra("copyFilename") != null);
		Log.i(TAG, "Should I start copying a file here? " + (hasCopy ? "Yes" : "No") + ", " + (copied ? "but I already did" : "let's go"));
		if (hasCopy && !copied) {
			
			// Folder and filename
			final String copyFolder = getIntent().getStringExtra("copyFolder");
			final String copySourceBrowse = getIntent().getStringExtra("copySourceBrowse");
			final String copyFilename = getIntent().getStringExtra("copyFilename");
			
			// Ask confirmation
			AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
			builder.setMessage(String.format(getString(R.string.action_file_copyto_confirm), copyFilename, copyFolder));
			builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
				@Override public void onClick(DialogInterface dialog, int which) {
					
					// Auto refresh to watch copy progress
					setAutoRefresh(true);
					
					// Start copying...
					suspendAutoRefresh();
					hideListOnNextError = false;
					Map<String, String> params = new HashMap<String, String>();
					params.put("folder", copyFolder);
					if (copySourceBrowse != null) params.put("sourcebrowse", copySourceBrowse);
					params.put("filename", copyFilename);
					getBerryBeeWebService().requestService(getServiceName(), "copy", makeBrowseParam(params));
					
				}
			});
			builder.setNegativeButton(R.string.no, null);
			builder.create().show();
			
			// Yes or no, do not do copy again
			copied = true;
			
		}
		
	}
	
	public void doMkdir() {
		if (!this.canMkdir) return;

		// Prompt directory name
		AlertDialog.Builder builder = new AlertDialog.Builder(getBerryBeeActivity());
		builder.setTitle(R.string.action_mkdir_prompt);
		final View v = getLayoutInflater().inflate(R.layout.dialog_edit_text, null);
		final EditText input = (EditText)v.findViewById(R.id.edit_text);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				suspendAutoRefresh();
				hideListOnNextError = false;
				Map<String, String> params = new HashMap<String, String>();
				params.put("sub", input.getText().toString());
				getBerryBeeWebService().requestService(getServiceName(), "mkdir", makeBrowseParam(params));
			}
		});
		builder.setInverseBackgroundForced(true);
		builder.setNegativeButton(R.string.cancel, null);
		builder.create();
		AlertDialog inputDialog = builder.create();
		inputDialog.setView(v);
		inputDialog.show();

	}
	
	public void showEmpty(boolean empty) {
		findViewById(R.id.list).setVisibility(empty ? View.GONE : View.VISIBLE);
		findViewById(R.id.empty).setVisibility(empty ? View.VISIBLE : View.GONE);
	}
	
	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.ls, menu);
		super.onCreateOptionsMenu(menu);
		return true;
	}

	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_mkdir:
				doMkdir();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_mkdir).setVisible(canMkdir);
		return true;
	}
	
	/** Calls super method only, do not inflate menu */
	public boolean callSuperOnCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		return true;
	}
	
	@Override protected void onSaveInstanceState(Bundle outState) {
		
		// Avoid asking for copy again if activity restarts
		outState.putBoolean("copied", copied);
		
		super.onSaveInstanceState(outState);
	}

	@Override protected void onRestoreInstanceState(Bundle savedInstanceState) {

		// Avoid asking for copy again if activity restarts
		copied = savedInstanceState.getBoolean("copied", false);
		
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override public void onBackPressed() {
		
		// If browsing a sub-folder, go up one level
		if (!browsing.isEmpty()) {
			browse("..");
			suspendAutoRefresh();
			refresh();
			
		} else super.onBackPressed();
	}
	
}
