package com.lorenzostanco.berrybeeclient.view;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.utils.JSONWebService;

public class XBMCActivity extends LogActivity {

	private boolean running = false;

	@Override protected void onCreate(Bundle savedInstanceState) {
		super.callSuperOnCreate(savedInstanceState);
		setContentView(R.layout.activity_xbmc);
		
		// Larger timeout for XBMC commands
		getBerryBeeWebService().setTimeout(30000);
		
		// Hide XBMC status initially
		findViewById(R.id.xbmc_wrapper).setVisibility(View.GONE);

		// Manage running stuffs
		setRunning(false);
		getBerryBeeWebService().addOnSuccessListener(new JSONWebService.OnSuccessListener() {
			@Override public void onSuccess(String url, JSONObject response) {

				// Now I can show status bar
				findViewById(R.id.xbmc_wrapper).setVisibility(View.VISIBLE);
				
				// Set running status
				setRunning(response.optString("status", "").indexOf("running") >= 0);
				
				// Display status details
				try {
					String status = response.getString("status");
					((TextView)findViewById(R.id.list_item_description)).setText(status);
					findViewById(R.id.list_item_description).setVisibility(View.VISIBLE);
				} catch (JSONException x) {
					findViewById(R.id.list_item_description).setVisibility(View.GONE);
				}
				
			}
		});

		// Setup standard Log stuff
		setupLogOptions();
		setupLogList();
		
	}

	public void start() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "start");
	}
	
	public void stop() {
		suspendAutoRefresh();
		getBerryBeeWebService().requestService(getServiceName(), "stop");
	}

	public void setRunning(boolean running) {
		this.running = running;
		
		// Refresh XBMC status
		((TextView)findViewById(R.id.list_item_text)).setText(isRunning() ? R.string.xbmc_running : R.string.xbmc_not_running);
		
		// Refresh menu
		if (this.getMenu() != null) onPrepareOptionsMenu(this.getMenu());
		
	}

	@Override public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_xbmc_start).setVisible(!isRunning());
		menu.findItem(R.id.action_xbmc_stop).setVisible(isRunning());
		return true;
	}

	public boolean isRunning() {
		return running;
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.xbmc, menu);
		super.callSuperOnCreateOptionsMenu(menu);
		return true;
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_xbmc_start:
				start();
				return true;
			case R.id.action_xbmc_stop:
				stop();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
}
