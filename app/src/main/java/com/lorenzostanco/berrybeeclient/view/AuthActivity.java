package com.lorenzostanco.berrybeeclient.view;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.lorenzostanco.berrybeeclient.BerryBeeActivity;
import com.lorenzostanco.berrybeeclient.BerryBeeAuthManager;
import com.lorenzostanco.berrybeeclient.BerryBeeAuthManager.Token;
import com.lorenzostanco.berrybeeclient.BerryBeeWebService;
import com.lorenzostanco.berrybeeclient.R;
import com.lorenzostanco.utils.JSONWebService;

public class AuthActivity extends ActionBarActivity implements 
	JSONWebService.OnRequestListener, 
	JSONWebService.OnCompleteListener,
	JSONWebService.OnErrorListener {
	
	private BerryBeeAuthManager authManager;
	private ListView list;
	private String deviceID;

	private MenuItem progressMenuItem = null;
	
	@SuppressLint("NewApi") @Override protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left_partial);

		// Add indeterminate progress indicator in activity title/action bar
		setRefreshActionButtonState(false);
		
		// Load layot
		setContentView(R.layout.activity_auth);
		final Activity self = this;
		
		// Up as home
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		// Device ID
		deviceID = android.os.Build.MODEL;
		((TextView)findViewById(R.id.text_device_id)).setText(deviceID);
		
		// Instantiate the auth manager
		this.authManager = new BerryBeeAuthManager(this);
		
		// Init list and its adapter
		final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = (ListView)findViewById(R.id.list_auth);
		list.setAdapter(new BaseAdapter() {
			
			@Override public View getView(int i, View convertView, ViewGroup parent) {
				BerryBeeAuthManager.Token token = getItem(i);
				if (convertView == null) convertView = inflater.inflate(token == null ? R.layout.list_item_auth_new : R.layout.list_item_auth, null);
				if (token != null) {
					TextView textView = (TextView)convertView.findViewById(R.id.list_item_text);
					textView.setText(token.getBaseURL());
					textView.setTypeface(null, isCurrentToken(token.getBaseURL(), token.getToken()) ? Typeface.BOLD : Typeface.NORMAL);
					((TextView)convertView.findViewById(R.id.list_item_description)).setText(token.getToken());
				}
				return convertView;
			}
			
			@Override public int getViewTypeCount() {
				return 2;
			}
			
			@Override public int getItemViewType(int i) {
				return i < getCount() - 1 ? 0 : 1;
			}
			
			@Override public BerryBeeAuthManager.Token getItem(int i) {
				return i < getCount() - 1 ? authManager.getTokens().get(i) : null;
			}
			
			@Override public int getCount() {
				return authManager.getTokens().size() + 1;
			}
			
			@Override public long getItemId(int i) {
				return i;
			}
			
		});

		// Items click
		list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override public void onItemClick(AdapterView<?> parent, View view, int i, long id) {
				
				// Click on an existing token?
				if (i < authManager.getTokens().size()) {
					Token token = authManager.getTokens().get(i);
					setCurrentToken(token.getBaseURL(), token.getToken());
					((BaseAdapter)list.getAdapter()).notifyDataSetChanged();
					new Timer().schedule(new TimerTask() {
						@Override public void run() { finish(); }
					}, 400); // Wait a moment to let the user see the current token has changed (bolded)
					
				// Click on "add new"?
				} else {
					final View dialogView = inflater.inflate(R.layout.dialog_auth, null);
					AlertDialog.Builder builder = new AlertDialog.Builder(self);
					builder.setTitle(R.string.auth_add_server_dialog_title);
					builder.setPositiveButton(R.string.auth, new DialogInterface.OnClickListener() {	
						@Override public void onClick(DialogInterface dialog, int which) {
							String baseURL = ((TextView)dialogView.findViewById(R.id.edit_base_url)).getText().toString();
							String passphrase = ((TextView)dialogView.findViewById(R.id.edit_passphrase)).getText().toString();
							tryAuthentication(baseURL, passphrase);
						}
					});
					builder.setInverseBackgroundForced(true);
					builder.setNegativeButton(R.string.cancel, null);
					builder.setView(dialogView);
					builder.create().show();
				}
				
			}
		});

		// Items long-click
		list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override public boolean onItemLongClick(AdapterView<?> parent, View view, int i, long id) {
				if (i < authManager.getTokens().size()) {
					final Token token = authManager.getTokens().get(i);
					AlertDialog.Builder builder = new AlertDialog.Builder(self);
					builder.setMessage(String.format(self.getString(R.string.auth_remove_server_confirm), token.getBaseURL()));
					builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {	
						@Override public void onClick(DialogInterface dialog, int which) {
							removeToken(token.getBaseURL(), token.getToken());
						}
					});
					builder.setNegativeButton(R.string.no, null);
					builder.create().show();
					return true;
				}
				return false;
			}
		});
		
	}

	public void tryAuthentication(String baseURL, String passphrase) {
		
		// Fix base URL and instantiate a new WS with no token
		final String finalBaseURL = (!baseURL.startsWith("http://") && !baseURL.startsWith("https://") ? "http://" : "") + baseURL;
		BerryBeeWebService ws = new BerryBeeWebService(finalBaseURL, "");
		ws.addOnRequestListener(this);
		ws.addOnCompleteListener(this);
		ws.addOnErrorListener(this);
		
		// On success, get token, store it and refresh list
		ws.addOnSuccessListener(new JSONWebService.OnSuccessListener() {	
			@Override public void onSuccess(String url, JSONObject response) {
				try {
					String token = response.getString("token");
					if (token == null || token.equals("")) throw new JSONException("");
					authManager.storeToken(finalBaseURL, token, AuthActivity.this);
					((BaseAdapter)list.getAdapter()).notifyDataSetChanged();
				} catch (JSONException e) {
					AuthActivity.this.onError(url, "", getString(R.string.error_response_unexpected));
				}
			}
		});

		// Request auth
		ws.auth(deviceID, passphrase);

	}

	public boolean isCurrentToken(String baseURL, String token) {
		
		// Check current token in shared preferences
		SharedPreferences preferences = getSharedPreferences(BerryBeeActivity.TAG, MODE_PRIVATE);
		String currentBaseURL = preferences.getString("baseURL", "");
		String currentToken = preferences.getString("token", "");
		return currentBaseURL.equals(baseURL) && currentToken.equals(token);
		
	}

	public void setCurrentToken(String baseURL, String token) {
		
		// Set current token in shared preferences
		Editor preferencesEditor = getSharedPreferences(BerryBeeActivity.TAG, MODE_PRIVATE).edit();
		preferencesEditor.putString("baseURL", baseURL);
		preferencesEditor.putString("token", token);
		preferencesEditor.commit();
		
	}

	public void removeToken(String baseURL, String token) {
		this.authManager.removeToken(baseURL, token, this);
		
		// If removed token is the current one, reset auth status
		SharedPreferences preferences = getSharedPreferences(BerryBeeActivity.TAG, MODE_PRIVATE);
		String currentBaseURL = preferences.getString("baseURL", "");
		String currentToken = preferences.getString("token", "");
		if (currentBaseURL.equals(baseURL) && currentToken.equals(token)) setCurrentToken("", "");
		
		((BaseAdapter)list.getAdapter()).notifyDataSetChanged();	
	}

	/** Shows/hides progress icon on the action bar */
	public void setRefreshActionButtonState(final boolean refreshing) {
	    if (progressMenuItem != null) progressMenuItem.setVisible(refreshing);
	}

	@Override public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.auth, menu);
		progressMenuItem = menu.findItem(R.id.progress);
		MenuItemCompat.setActionView(progressMenuItem, R.layout.actionbar_indeterminate_progress);
		setRefreshActionButtonState(false);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override public void onComplete(String url) {
		Log.i(BerryBeeActivity.TAG, "[WS] Complete: " + url);
		setRefreshActionButtonState(false);
	}

	@Override public void onRequest(String url) {
		Log.d(BerryBeeActivity.TAG, "[WS] Request: " + url);
		setRefreshActionButtonState(true);
	}

	@SuppressLint("NewApi") @Override public void onError(String url, String type, String message) {
		Log.e(BerryBeeActivity.TAG, "[WS] " + type + ": " + url);
		if (message == null || message.equals("")) message = "Unknown error.";
		if (type.equals("MalformedURLException")) message = getString(R.string.error_url_malformed);
		if (type.equals("UnknownHostException")) message = getString(BerryBeeActivity.isNetworkAvailable(AuthActivity.this) ? R.string.error_unknown_host : R.string.error_network_unavailable);
		if (type.equals("JSONException")) message = getString(R.string.error_invalid_json);
		if (type.equals("FileNotFoundException")) message = getString(R.string.error_not_found);
		AlertDialog.Builder builder = new AlertDialog.Builder(AuthActivity.this);
		builder.setTitle(R.string.error).setMessage(message);
		builder.setPositiveButton(android.R.string.ok, null);
		if (android.os.Build.VERSION.SDK_INT >= 11) builder.setIconAttribute(android.R.attr.alertDialogIcon);
		else builder.setIcon(android.R.drawable.ic_dialog_alert);
		builder.create().show();
	}

	@Override public void finish() {
		super.finish();
		overridePendingTransition(R.anim.slide_in_left_partial, R.anim.slide_out_right);
	}

	@Override protected void onDestroy() {
		super.onDestroy();
		overridePendingTransition(R.anim.slide_in_left_partial, R.anim.slide_out_right);
	}

}